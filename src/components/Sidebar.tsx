/* eslint-disable @next/next/no-img-element */
import React from 'react';
import Image from 'next/image';
import {AiFillGitlab,AiFillLinkedin} from 'react-icons/ai';
import {GoLocation} from 'react-icons/go';
import {GiTie} from 'react-icons/gi';
import {useTheme} from 'next-themes'


const Sidebar = () => {
    const {theme,setTheme} = useTheme();
    const changeTheme = () => {
        setTheme(theme==='light' ? 'dark': 'light')
    }
    return (
        <div>
            <Image 
                src="/images/profil.jpeg"
                alt="user avat"
                className="mx-auto rounded-full"
                layout='intrinsic' 
                width={128} 
                height={128}
                quality='100'
            />
            <h3 className="my-3 text-3xl font-medium tracking-wider font-kaushan">
                <span className="text-green">Tandrify </span>
                Miadamampionona
            </h3>
            <p className="px-2 py-1 my-3 bg-gray-200 rounded-full dark:bg-dark-200"> Développeur fullstack javascript</p>
            <a 
                href='/doc/cv_tandrify.pdf' 
                download="name" 
                className="flex items-center justify-center px-2 py-1 my-3 bg-gray-200 rounded-full dark:bg-dark-200"
            > 
                <GiTie className="w-6 h-6"/>
                Téléchargez mon CV
            </a>
            {/** social icons */}
            <div className="flex justify-around w-9/12 mx-auto my-5 text-green md:w-full">
                <a href="https://gitlab.com/tandrify">
                    <AiFillGitlab className="w-8 h-8 cursor-pointer"/>
                </a>
                <a href="https://www.linkedin.com/in/tandrify-miadamampionona-a3450316a">
                    <AiFillLinkedin className="w-8 h-8 cursor-pointer"/>
                </a>
            </div>
            {/**address */}
            <div className="py-4 my-5 bg-gray-200 dark:bg-dark-200" style={{marginLeft:'-1rem',marginRight:'-1rem'}}>
                <div className="flex items-center justify-center space-x-2">
                    <GoLocation />
                    <span>Antananarivo, Madagascar</span>
                </div>
                <p className="my-2">tandrify@gmail.com</p>
                <p className="my-2">+261 34 13 783 43</p>
            </div>
            {/** email button */}
            <button 
                className="w-8/12 px-5 py-2 my-2 text-white rounded-full bg-gradient-to-r from-green to-blue-400 focus:outline-none"
                onClick={() => window.open('mailto:trandrify@gmail.com')}
                >
                Envoyez-moi un email
            </button>
            <button onClick={changeTheme} className="w-8/12 px-5 py-2 my-2 text-white rounded-full bg-gradient-to-r from-green to-blue-400 focus:outline-none">
                {theme === 'light' ? 'Dark mode' : 'Light mode'}
            </button>
        </div>
    )
}

export default Sidebar;
