import React,{FC} from 'react'
import { Category } from '../../type'

interface INavbarItem {
    value:Category | 'all';
    active:Category | 'all';
    handleCategory:(category:Category | 'all') => void;

}

export const NavbarItem: FC<INavbarItem>= ({value,handleCategory,active}) => {
    let className = 'capitalize cursor-pointer hover:text-green';
    let activeClassName = className + ' text-green'
    return (
        <li
            onClick={() => handleCategory(value)}
            className={active === value ? activeClassName : className}
        >
            {value}
        </li>
    ) 
}

interface IProjectNavbar {
    handleCategory:(category:Category | 'all') => void;
    active: Category |'all';
}


const ProjectsNavbar: FC<IProjectNavbar> = ({handleCategory,active}) => {
    return (
        <div className="flex px-3 py-2 space-x-3 overflow-x-auto list-none">
            <NavbarItem  value={'all'}  handleCategory={handleCategory} active={active}/>
            <NavbarItem  value={'react'} handleCategory={handleCategory} active={active}/>
            <NavbarItem  value={'next'} handleCategory={handleCategory} active={active}/>
            <NavbarItem  value={'react native'} handleCategory={handleCategory} active={active}/>
            <NavbarItem  value={'node'} handleCategory={handleCategory} active={active}/>
            <NavbarItem  value={'express'} handleCategory={handleCategory} active={active}/>
        </div>
    )
}

export default ProjectsNavbar
