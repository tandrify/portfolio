/* eslint-disable @next/next/no-img-element */
import React,{FC, useState} from 'react'
import { AiFillGitlab, AiFillProject } from 'react-icons/ai'
import { IProject } from '../../type'
import {MdClose} from 'react-icons/md'
import Image from 'next/image'
import {motion} from 'framer-motion'
import { stagger } from '../../animation'
import { fadinUp } from '../../animation';

interface IProjectCard {
    project:IProject
    setShowDetail: (id:number |null) => void
    showDetail: number |null
}
const ProjectCard:FC<IProjectCard> = ({project:{id,name,description,image_url,deployed_url,github_url,category,key_techs},setShowDetail,showDetail}) => {
    
    return (
        <div>
            <Image 
                layout='responsive' 
                width={300} 
                height={150} 
                alt={name} 
                src={image_url} 
                className="cursor-pointer" 
                onClick={() => setShowDetail(id)}
            />
            <p className="my-2 text-center">{name}</p>

            {(showDetail && showDetail === id) && (
                <div className="absolute top-0 left-0 z-10 grid w-full h-auto p-2 bg-gray-100 rounded-lg md:p-10 md:grid-cols-2 gap-x-12 text-balck dark:text-white dark:bg-dark-100">
                    <motion.div variants={stagger} initial="initial" animate="animate">
                        <motion.div variants={fadinUp} className="border-4 border-gray-100">
                            <Image 
                                layout='responsive' 
                                width={300} 
                                height={150} 
                                alt={name} 
                                src={image_url}
                            />
                        </motion.div>
                        
                        <motion.div className="flex justify-center my-4 space-x-3" variants={fadinUp}>
                            <a href={github_url} rel="noreferrer" target="_blank" className="flex px-4 py-2 space-x-3 text-lg bg-gray-100 item-center dark:bg-dark-100">
                                <AiFillGitlab className="w-8 h-8 cursor-pointer"/>
                                <span>Gitlab</span>
                            </a>
                            <a href={deployed_url} rel="noreferrer" target="_blank" className="flex px-4 py-2 space-x-3 text-lg bg-gray-100 item-center dark:bg-dark-100">
                                <AiFillProject className="w-8 h-8 cursor-pointer"/>
                                <span>Démo</span>
                            </a>
                        </motion.div>
                    </motion.div>
                    
                    <motion.div variants={stagger} initial="initial" animate="animate">
                        <motion.h2 variants={fadinUp} className="mb-3 text-xl font-medium md:text-2xl">{name}</motion.h2>
                        <motion.h3 variants={fadinUp} className="mb-3 font-medium">{description}</motion.h3>
                        <motion.div variants={fadinUp} className="flex flex-wrap mt-5 space-x-2 text-sm tracking-wider">
                            {key_techs.map((tech,index) => (
                                <span className="px-2 py-1 my-1 bg-gray-200 rounded-sm dark:bg-dark-200 darl:bg-dark-200" key={index}>{tech}</span>
                            ))}
                        </motion.div>
                    </motion.div>

                    <button
                        className="absolute p-1 bg-gray-200 rounded-full top-3 right-3 focus:outline-none dark:bg-dark-200"
                        onClick={() => setShowDetail(null)}
                    >
                        <MdClose size={30}/>
                    </button>

                </div>
            )}

            

        </div>
    )
}

export default ProjectCard
