import React,{FC} from 'react'
import { ISkill } from '../../type'
import {motion} from 'framer-motion'

interface IBar{
    language:ISkill
}

const Bar:FC<IBar> = ({language:{name,Icon,level}}) => {
    const variants = {
        intitial:{
            width:0
        },
        animate:{
            width:level,
            transition:{
                duration:0.5,
                type:'spring',
                damping:10,
                stiffness:100
            }
        }
    }
    return (
        <div className="my-2 text-white bg-gray-300 rounded-full dark:bg-dark-200">
            <motion.div 
                className="flex items-center px-4 py-1 rounded-full bg-gradient-to-r from-green to-blue-600" 
                style={{width:level}}
                variants={variants}
                initial='intitial'
                animate='animate'
            >
                <Icon className="mr-3"/>
                {name}
            </motion.div>
        </div>
    )
}

export default Bar
