import React from 'react'
import { languages, tools } from '../../data'
import Bar from '../components/Bar'
import {motion} from 'framer-motion'
import { fadinUp, route } from '../../animation'

const resume = () => {
    
    return (
        <motion.div className='p-4' variants={route} initial="initial" animate="animate" exit="exit">
            {/** euducation & exp */}
            <div className="grid gap-6 md:grid-cols-2">
                <motion.div variants={fadinUp} initial="initial" animate="animate">
                    <h5 className="my-3 text-2xl font-bold"> Education </h5>
                    <div>
                        <h5 className="my-2 text-base font-semibold"> Génie logiciel & base de donnée (2019-2021)</h5>
                        <p className="text-sm font-semibold text-gray-500"> Autodidacte</p>
                        <h5 className="my-2 text-base font-semibold">Réseau et système (2014-2019)</h5>
                        <p className="text-sm font-semibold text-gray-500">École Supérieure Polytechnique d’Antananarivo </p>
                    </div>
                </motion.div>

                <motion.div variants={fadinUp} initial="initial" animate="animate">
                    <h5 className="my-3 text-2xl font-bold"> Expériences </h5>
                    <div>
                        {/*eslint-disable-next-line react/no-unescaped-entities*/}
                        <h5 className="my-2 text-base font-semibold"> DÉVELOPPEUR FULLSTACK JAVASCRIPT 08/2020 - Aujourd'hui</h5>
                        <p className="text-sm font-semibold text-gray-500"> active dans 3 projets en React/React Native, GraphQL</p>
                        <h5 className="my-2 text-base font-semibold">STAGIAIRE DÉVELOPPEUR JAVA 11/2018 - 02/2019</h5>
                        <p className="text-sm font-semibold text-gray-500">Création d’une application Java pour compléter le logiciel Tessi Docubase </p>
                    </div>
                </motion.div>
            </div> 
            {/** languge & tools */}
            <div className="grid gap-6 my-5 md:grid-cols-2">
                <div>
                    <h5 className="my-3 text-2xl font-bold"> Langages & Framewoks </h5>
                    <div className="my-2">
                        {languages.map((language,index) => <Bar key={index} language={language}/>)}
                    </div>
                </div>

                <div>
                    <h5 className="my-3 text-2xl font-bold"> Outils & logiciels </h5>
                    <div className="my-2">
                        {tools.map((language,index) => <Bar key={index} language={language}/>)}
                    </div>
                </div>
            </div>
        </motion.div>
    )
}

export default resume
