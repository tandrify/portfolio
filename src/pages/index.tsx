import React from 'react'
import { fadinUp, route, stagger } from '../../animation';
import {services} from '../../data'
import ServiceCard from '../components/ServiceCard'
import {motion} from 'framer-motion'

const index = () => {
  return (
    <motion.div 
      className="flex flex-col flex-grow px-6 pt-1" 
      variants={route} 
      initial="initial" 
      animate="animate" 
      exit="exit"
    >
      <h5 className="my-3 font-medium"> {"Je suis curieux et autodidacte. J’ai une appétence pour les nouvelles technologies"} </h5>
      <div className="flex-grow p-4 mt-5 bg-gray-400 dark:bg-dark-100" style={{marginLeft:'-1.5rem',marginRight:'-1.5rem'}}>
        <h6 className="my-3 text-xl font-bold tracking-wide"> Ce que je fais </h6>
        <motion.div className="grid gap-6 lg:grid-cols-2" variants={stagger} initial="initial" animate="animate">
          {services.map((service,index) =>
            <motion.div key={index} className="bg-gray-200 rounded-lg dark:bg-dark-200 lg:col-span-1" variants={fadinUp}>
                <ServiceCard  service={service}/>
            </motion.div>
          )}
        </motion.div>
      </div>
    </motion.div>
  )
}
export default index;
