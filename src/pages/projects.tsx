import React, { useState } from 'react'
import { projects as projectsData} from '../../data'
import ProjectCard from '../components/ProjectCard'
import ProjectsNavbar from '../components/ProjectsNavbar'
import { Category, IProject } from '../../type'
import {motion} from 'framer-motion'
import { fadinUp, route, stagger } from '../../animation'

const Projects = () => {
    
    const [projects,setProjects] = useState<IProject[]>(projectsData);
    const [active,setActive] = useState<Category | 'all'>('all');
    const [showDetail,setShowDetail] = useState<number | null>(null);
    
    const handleCategory = (category:Category | 'all') => {
        if(category ==='all') {
            setProjects(projectsData);
            setActive('all');
            return;
        }
        
        const filteredProject =  projectsData.filter(project => project.category.includes(category));
        setProjects(filteredProject);
        setActive(category);
    }
    
    return (
        <motion.div 
            className="px-5 py-2 overflow-y-scroll" 
            style={{height:'65vh'}} 
            variants={route} 
            initial="initial" 
            animate="animate" 
            exit="exit"
        >
            <ProjectsNavbar handleCategory={handleCategory} active={active}/>
            <motion.div className="relative grid grid-cols-12 gap-4 my-3" variants={stagger} initial="initial" animate="animate">
                {projects.map((project,index) => (
                    <motion.div 
                        className="col-span-12 bg-gray-200 rounded-lg sm:col-span-6 lg:col-span-4 dark:bg-dark-200" 
                        key={index}
                        variants={fadinUp}
                    >
                        <ProjectCard project={project} showDetail={showDetail} setShowDetail={setShowDetail}/>
                    </motion.div>
                ))}
            </motion.div>
        </motion.div>
    )
}

export default Projects
