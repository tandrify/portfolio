import {RiComputerLine} from 'react-icons/ri';
import { IService, ISkill, IProject } from './type';
import {AiOutlineApi} from 'react-icons/ai';
import {FaServer} from 'react-icons/fa';
import {BsCircleFill} from 'react-icons/bs';


export const services:IService[] = [
    {
        title:'Developpement frontend',
        about: 'React, Next, React Native',
        Icon:RiComputerLine
    },
    {
        title:"Developpement d'API",
        about: 'REST API avec NodeJS, GraphQL API',
        Icon:AiOutlineApi
    },
    {
        title:"Developpement backend",
        about: 'NodeJS, Express',
        Icon:RiComputerLine
    },
]

export const languages:ISkill[] = [
    {
        name:'Javascript',
        level: '70%',
        Icon:BsCircleFill
    },
    {
        name:'Typescript',
        level: '70%',
        Icon:BsCircleFill
    },
    {
        name:'Java',
        level: '30%',
        Icon:BsCircleFill
    },
    {
        name:'React',
        level: '80%',
        Icon:BsCircleFill
    },
    {
        name:'React native',
        level: '82%',
        Icon:BsCircleFill
    },
    {
        name:'NodeJS',
        level: '70%',
        Icon:BsCircleFill
    },
]

export const tools:ISkill[] = [
    {
        name:'Jetbrain/Datagrip',
        level: '70%',
        Icon:BsCircleFill
    },
    {
        name:'Inkscape',
        level: '30%',
        Icon:BsCircleFill
    },
]

export const projects:IProject[] = [
    {   id:1,
        name: "Amazone clone",
        description: "Clone d'Amazon avec paiement stripe",
        image_url:'/images/next-amazone.png',
        deployed_url:'https://next-ti-ta-ekomasu.vercel.app/',
        github_url:'https://gitlab.com/MamihenintsoaRakotobe/next-amazone-clone',
        category:['next','react'],
        key_techs:['NextJS','Stripe',"TailwindCSS","Firebase","NextAuth","redux"]
    },
]